
// ****************Retrieve ALL
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => console.log(json));

// ****************MAP

fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	let titleMap = title.map(dataMap => {
		return{
			title:dataMap.title
		};
	});

	console.log(titleMap);

});

//**************** Retrieve Single
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(result => console.log(result));



//**************** 6. Print Message

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(json => console.log(`The items "${json.title}" on the list has a status of ${json.completed}`))




// ****************7. Fetch create

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Created to do list item",
		id: 201,
		userId:1
		
	})
})
.then(res => res.json())
.then(json => console.log(json));




// ****************8 Fetch update
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Correctred Post",
		datecompleted: "Pending",
		description: "To update the my to do list with a different data structure"
	})
})
.then(res => res.json())
.then(json => console.log(json));



//**************** 9. Update to do list

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "update to do list",
		description: "To update the my to do list with a different data structure",
		status: "updated",
		date_completed: "today",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));



//**************** 10. Update using patch

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "done updating",
		description: "done updating",
		status: "done updating",
		date_completed: "Pending",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// **************** 11. Update status and add date

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		status: "complete",
		date_completed: "03-05-23",
	})
})
.then(res => res.json())
.then(json => console.log(json));




// **************** 12. DELETE an item

fetch("https://jsonplaceholder.typicode.com/todos/2", {
	method: "DELETE"
	
});

console.log("userId 2, deleted");



// **************** 12. DELETE an item




