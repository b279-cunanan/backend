const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required : [true, "FIRST NAME is required!"]
	},
	lastName : {
		type: String,
		required : [true, "LAST NAME is required!"]
	},
	email : {
		type: String,
		required : [true, "EMAIL is required!"]
	},
	password : {
		type: String,
		required : [true, "PASSWORD is required!"]
	},
	isAdmin : {
		type: Boolean,
		default : false
	},
	orderedProduct :[
						{
							products :[
											{
												productId : {
													type: String,
													required : [true, "PRODUCT ID is required!"]
												},
												productName : {
													type: String,
													required : [true, "PRODUCT NAME is required!"]										
												},
												quantity : {
													type: Number,
													required : [true, "PRODUCT QUANTITY is required!"]
													
												}
											}
									  ],
							
							
							totalAmount : {
								type: Number,
								default : 0
							},
							purchasedOn : {
								type: Date,	
								default : new Date()
							}
						}
					]


});



module.exports = mongoose.model("User", userSchema);