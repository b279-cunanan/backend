const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth.js");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then( resultFromController => res.send(resultFromController))
});


// USER REGISTRATION
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then( resultFromController => res.send(resultFromController))
});

// USER AUTHENTICATION
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then( resultFromController => res.send(resultFromController))
})

// RETRIEVE USER DETAILS
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.detailsUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// RETRIEVE USER ORDER DETAILS
router.post("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.orderDetailsUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// USERS LIST
router.get("/", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController));
});


// CHECKOUT product of non-admin user 
router.post("/checkout", auth.verify,(req, res) => {
	let data = {
		
		products : req.body
	}

	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let userIdAuth = auth.decode(req.headers.authorization).id;
	
	userController.checkout(data, userIdAuth, isAdmin).then(resultFromController => res.send(resultFromController));
})





// SET USER AS ADMIN
router.put("/updatestatus", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.updateUser(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})



// PRODUCT ADD TO CART of non-admin User 

router.post("/addToCart", auth.verify, (req, res) => {
	let data = {
		
		productId : req.body.productId,
		quantity : req.body.quantity
	}

	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let userIdAuth = auth.decode(req.headers.authorization).id;
	
	userController.addToCart(data, userIdAuth, isAdmin).then(resultFromController => res.send(resultFromController));
})


// PRODUCT REMOVE IN CART of non-admin User

router.post("/removeProductInCart", auth.verify,(req, res) => {
	let data = {
		
		id : req.body.id,
		
	}

	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let userIdAuth = auth.decode(req.headers.authorization).id;
	
	userController.removeProductInCart(data, userIdAuth, isAdmin).then(resultFromController => res.send(resultFromController));
})


// // Allows us to export the "router" object that will be access in our index.js file
module.exports = router;

