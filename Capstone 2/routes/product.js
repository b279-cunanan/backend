const express =require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");



// Create PRODUCT
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	
});

// Get all products
router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));
});

// Get all "ACTIVE" products
router.get("/", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllActive(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})

// Update a product
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})




// Archive a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Activate a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve ALL ORDERS
router.post("/orders", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));

})



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;


