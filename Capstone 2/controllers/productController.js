const Product = require("../models/Product.js");

// Create a new product
// module.exports.addProduct = (reqBody) => {


module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

// retrieve all products function
module.exports.getAllProducts = (isAdmin) => {
	return Product.find({}).then(result => {
		console.log(isAdmin);

		if(isAdmin){
			return result;
		}

		let message = Promise.resolve("You don't have the access rights to do this action.");

		return message.then((value) => {
			return value
		})	
	})
};


// retrieve all active products function
module.exports.getAllActive = (isAdmin) => {
	return Product.find({isActive : true}).then(result => {
		if(isAdmin){
			return result;
		}

		let message = Promise.resolve("You don't have the access rights to do this action.");

		return message.then((value) => {
			return value
		})
		
	})
}

// Retrieving a specific product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}

// Update a product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product Updated!";
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

	
}

// Archive product

module.exports.archiveProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		isActive : "false"
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "PRODUCT ARCHIVED" 
					
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

	
}

// ACTIVATE product

module.exports.activateProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		isActive : "true"
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			
			if(error){
				return false;
			}else{
				
				return "PRODUCT ACTIVATED"
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

	
}


// retrieve all orders 
module.exports.getAllOrders = (isAdmin) => {
	return Product.find().gt("__v", 0).then(result => {
		
		if(isAdmin){
			return result;
		}

		let message = Promise.resolve("You don't have the access rights to do this action.");

		return message.then((value) => {
			return value
		});

	})
}
		
