const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js")



// Check if the email already exist

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		} else {
			return false
		}
	})

}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName, 
		email: reqBody.email,
		// 10 is the value provided as the number of salt rounds
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else {
			return true;
		}
	})
};


// Function to login a user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if (result == null) {
			return false
		}else {
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if(isPasswordCorrect) {
					return {access: auth.createAccessToken(result)}
				}else {
					return false
				}
			  }
	})

};

// Get ALL user info for User ID
module.exports.getAllUser = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// Retrieve details of a user
module.exports.detailsUser = (data) => {
	return User.findById(data.userId).then(result => {
		if (result == null) {
			return false
		} else {

			result.password = ""
			
			return result
				} 
	})
}

// Retrieve ORDER DETAILS of a user
module.exports.orderDetailsUser = (data) => {
	return User.findById(data.userId).then(result => {
		if (result == null) {
			return false
		} else {
			
			return result.orderedProduct
				} 
	})
}
	


// Product checkout of a non-admin User 




module.exports.checkout = async (data, userIdAuth, isAdmin) => {



	if(isAdmin == false){

		let productTemp = [];
		let totalAmountTemp = 0;
		let isProductUpdated;


// Temp storage of each products

		data.products.forEach(singleProduct => {

		    console.log(singleProduct);
		    Product.findById(singleProduct.productId).then(product => {
		        productTemp.push(
		            {
		                productId : singleProduct.productId,
		                productName : product.name,
		                quantity : singleProduct.quantity
		            }
		        )
		        
		        totalAmountTemp = totalAmountTemp + (product.price * singleProduct.quantity)
		        console.log (totalAmountTemp)
		    })


// Operations on Poduct and USER
		    

		isProductUpdated =  Product.findById(singleProduct.productId).then(product =>{

				// console.log(product);
		        product.userOrders.push({userId : userIdAuth,
		    							 orderId : data.products.productId});

		        return product.save().then((product, error) => {
		            if(error){
		                return false;
		            } else {
		                return true;
		            }
		        })
		    })
		});


		let isUserUpdated = await User.findById(userIdAuth).then(user => {
                user.orderedProduct.push(
                    {

                        products : productTemp,
                        totalAmount : totalAmountTemp
                    }
                );
                return user.save().then((user, error) => {
                    if(error){
                        return false;
                    }else{
                        return true;
                    }
                })
        })




// RETURN MESSAGES

		if(isProductUpdated && isUserUpdated){
			return "Product checkout successful";
		}else{
			return "Something went wrong with your request. Please try again later";
		}

		

	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

		return message.then((value) => {
			return value
		})
}





// // Product ADD TO CART non-admin User 


// module.exports.addToCart = async (data, userIdAuth, isAdmin) => {

// console.log(isAdmin);

// // Get product info, new variable
// 		let productInfo =  await Product.findById(data.productId).then(result =>{
// 					return result;
// 		});

// 		console.log(productInfo)

// // checkout operataion
// 	if(isAdmin == false){
		
// // AWAIT || PRODUCT UPDATE
// 		let isProductUpdated = await Product.findById(data.productId).then(product => {

// 			product.userOrders.push({userId : userIdAuth,
// 									 orderId : data.productId});

// 			return product.save().then((product, error) => {
// 				if(error){
// 					return false;
// 				}else{
// 					return true;
// 				}
// 			})

// 		})


// // AWAIT || USER UPDATE
// 		let isUserUpdated = await User.findById(userIdAuth).then(user => {

// 		user.orderedProduct.push({
// 									products :
// 													{
// 														productId: data.productId,
// 														productName : productInfo.name,
// 														quantity : data.quantity
// 													},
							
// 								totalAmount : productInfo.price * data.quantity
// 		});

// 			return user.save().then((user, error) => {
// 				if(error){
// 					return false;
// 				}else{
// 					return true;
// 				}
// 			})
// 		});


// // RETURN MESSAGES
// 		if(isProductUpdated && isUserUpdated){
// 			return User.findById(userIdAuth).then(result => {
// 				if (result == null) {
// 					return false
// 				} else {
					
// 					return result.orderedProduct
// 						} 
// 			})
// 		}else{
// 			return "Something went wrong with your request. Please try again later";
// 		}

		

// 	}

// 	let message = Promise.resolve("You don't have the access rights to do this action.");

// 		return message.then((value) => {
// 			return value
// 		})
// }











// // Product REMOVE IN CART non-admin User 
// module.exports.removeProductInCart = async (data, userIdAuth, isAdmin) => {
// 	console.log(data.id);
	

// 	let userOrderedProduct =  await User.findById(userIdAuth).then(result =>{
// 					return result.orderedProduct;
// 		});

// 	console.log(userOrderedProduct)

// 	let orderedProduct =  await User.find({_id : data.id}).then(result =>{
// 					return result;
// 		});

// 	console.log(orderedProduct)

// 	if(isAdmin == false){

// 		// userOrderedProduct.findById({data.id}).then((order, error) => {
// 		// 	console.log(order)
// 		// 	if(error){
// 		// 		return false;
// 		// 	}
		

// 		// 		return userAccount
			
			
// 		// })



		
// 		// await User.findByIdAndRemove({_id: data.id}).then((error) => {
// 		// 	if(error){
// 		// 		return false;
// 		// 	}
		

// 		// return User.findById(userIdAuth).then(result => {
// 		// 	if (result == null) {
// 		// 		return false
// 		// 	} else {
				
// 		// 		return result.orderedProduct
// 		// 			} 
// 		// }) 
			
			
// 		// })
// 	}
	

// 	let message = Promise.resolve("You don't have the access rights to do this action.");

// 	return message.then((value) => {
// 		return value
// 	})

	
// }

// module.exports.deleteTask = (taskId) => {
// 	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
// 		if(error){
// 			console.log(error);
// 			return false;
// 		}else{
// 			return removedTask;
// 		}
// 	})
// }











// SET USER AS ADMIN
module.exports.updateUser = (reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedUser = {
		isAdmin : reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqBody.userId, updatedUser).then((user, error) => {
			if(error){
				return false;
			}else{
				return "User admin status updated!";
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})

	
}









