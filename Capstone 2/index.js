// SERVER CREATION

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const productRoutes = require("./routes/product.js");



const app = express();


mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.tr9npll.mongodb.net/E_Commerce_API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});


mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."))

// MIDDLEWARES
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Defines the "/users" string to be included for the user routes defined in the "user.js" route file
app.use("/users", userRoutes);
app.use("/products", productRoutes);


// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online in port ${process.env.PORT || 4000}`));
}

module.exports = app;