const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth.js");


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then( resultFromController => res.send(resultFromController))
});


// route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then( resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then( resultFromController => res.send(resultFromController))
})


router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.detailsUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


router.get("/", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController));
});


// Enroll user to a course
// router.post("/enroll", (req, res) => {
// 	let data = {
// 		userId : req.body.userId,
// 		courseId : req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// })


// ACTIVITY Enroll non-admin user to a course
router.post("/enroll", auth.verify,(req, res) => {
	let data = {
		
		courseId : req.body.courseId
	}

	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let userIdAuth = auth.decode(req.headers.authorization).id;
	
	userController.enroll(data, userIdAuth, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be access in our index.js file
module.exports = router;