const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/Course.js")



// Check if the email already exist

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		} else {
			return false
		}
	})

}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName, 
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of salt rounds
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else {
			return true;
		}
	})
};


// Function to login a user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if (result == null) {
			return false
		}else {
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if(isPasswordCorrect) {
					return {access: auth.createAccessToken(result)}
				}else {
					return false
				}
			  }
	})

};

// Get ALL user info for User ID
module.exports.getAllUser = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// Retrieve details of a user
module.exports.detailsUser = (data) => {
	return User.findById(data.userId).then(result => {
		if (result == null) {
			return false
		} else {

			result.password = " "
			
			return result
				}
			})
		}
	


// Enroll User to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/

// module.exports.enroll = async (data) => {
// 	// Add the course ID in the enrollments array of the user
// 	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
// 	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend

// 	let isUserUpdated = await User.findById(data.userId).then(user => {
// 		user.enrollments.push({courseId: data.courseId});

// 		return user.save().then((user, error) => {
// 			if(error){
// 				return false;
// 			}else{
// 				return true;
// 			}
// 		})
// 	})

// 	// Add the user ID in the enrollees array of the course
// 	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend

// 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

// 		course.enrollees.push({userId : data.userId});

// 		return course.save().then((course, error) => {
// 			if(error){
// 				return false;
// 			}else{
// 				return true;
// 			}
// 		})

// 	})

// 	// Condition that will check if the user and course documents has been updated
// 	// User enrollment is successful

// 	if(isUserUpdated && isCourseUpdated){
// 		return "You are now enrolled";
// 	}else{
// 		return "Something went wrong with your request. Please try again later";
// 	}
// }




// Enroll a non-admin User to a class


module.exports.enroll = async (data, userIdAuth, isAdmin) => {

console.log(isAdmin);

	if(isAdmin == false){
		

		let isCourseUpdated = await Course.findById(data.courseId).then(course => {

			course.enrollees.push({userId : userIdAuth});

			return course.save().then((course, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			})

		})

		let isUserUpdated = await User.findById(userIdAuth).then(user => {
		user.enrollments.push({courseId: data.courseId});

			return user.save().then((user, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			})
		});



		if(isCourseUpdated){
			return "You are now enrolled";
		}else{
			return "Something went wrong with your request. Please try again later";
		}

		

	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

		return message.then((value) => {
			return value
		})
}